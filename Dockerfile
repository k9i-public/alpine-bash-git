FROM alpine:3.20.3

RUN set -eux \
    && apk update --no-cache \
    && apk add --no-cache \
    bash \
    git \
    curl \
    tree \
    && true

CMD [ "/bin/bash" ]
