#!/bin/sh
set -eu

IMAGE_NAME=${IMAGE_NAME:-registry.gitlab.com/k9i-public/alpine-bash-git}
IMAGE_TAG=${IMAGE_TAG:-latest}
DOCKERFILE=${DOCKERFILE:-./Dockerfile}
DOCKER_PATH=${DOCKER_PATH:-.}

cmd=${1:-}
case ${cmd} in
  build)
    ( set -x; docker build -f ${DOCKERFILE} -t ${IMAGE_NAME}:build ${DOCKER_PATH} )
    ( set -x; docker tag ${IMAGE_NAME}:build ${IMAGE_NAME}:latest )
    ( set -x; docker tag ${IMAGE_NAME}:build ${IMAGE_NAME}:${IMAGE_TAG} )
    ;;
  push)
    ( set -x; docker push ${IMAGE_NAME}:${IMAGE_TAG} )
    ( set -x; docker push ${IMAGE_NAME}:latest )
    ;;
  run)
    ( set -x; docker run --rm -it ${IMAGE_NAME}:${IMAGE_TAG} )
    ;;
  *)
    1>&2 echo "$0: unknown command='$cmd'; exit=78" && exit 78
    ;;
esac
